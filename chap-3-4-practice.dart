
import 'dart:io';

import 'dart:isolate';

void main() async {
  // Constructors and getters, setters
  Person somePerson = Person("Clark", "Kent");
  print(somePerson.getFullName()); // prints Clark Kent
  print(somePerson.initials);
  print(somePerson.getFullName());
  somePerson.fullName = 'Vince Marcus';
  print(somePerson.fullname);
  print('\n');

  // Static attribute
  Person.personLabel = 'Dr.';
  print(somePerson.fullname);
  
  // inheritance
  Student student = Student("Clark", "Kent", "Kal-El");
  print(student);

  // Enum
  print(PersonType.values);
  Person somePerson1 = Person('Jon', 'Ander');
  somePerson1.type = PersonType.employee;
  print(somePerson1.type);
  print(somePerson1.type?.index);

  // Generics
  //List<String> placeNames = ["Middlesbrough", "New York"];
  //placeNames.add(1); -> doesn't work since it is already defined as string type list
  var placeNames = <String>["Middlesbrough", "New York"];
  var landmarks = <String, String?>{"Middlesbrough": null, "New York": "Statue of Liberty",};
  var emptyStringArray = <String>[];
  
  // Asynchronous
  print("start of long running operation");
  longRunningOperation();
  print("continuing main body");
  for (int i = 10; i < 15; i++) {
    await Future.delayed(Duration(seconds: 1));
    print("index from main: $i");
  }
  print("end of main");

  // Isolates
  Isolate.spawn(longRunningOperation1, "Hello");
  print("continuing main body");
  for (int i = 10; i < 15; i++) {
    await Future.delayed(Duration(seconds: 1));
    print("index from main: $i");
  }
  print("end of main");
}

Future<void> longRunningOperation1(String message) async {
  for (int i = 0; i < 5; i++) {
  await Future.delayed(Duration(seconds: 1));
  print("$message: $i");
  }
}

Future<void> longRunningOperation() async {
  for (int i = 0; i < 5; i++) {
    await Future.delayed(Duration(seconds: 1));
    print("index: $i");
  }
}

class Person {
  late String firstName;
  late String lastName;
  static String personLabel = "Mr.";
  
  PersonType? type;

  Person(this.firstName, this.lastName);


  
  String getFullName() => "$firstName $lastName";
  String get initials => "${firstName[0]}.${lastName[0]}.";
  String get fullname => "$personLabel $firstName $lastName";

  set fullName(String fullName) {
    var parts = fullName.split(" ");
    this.firstName = parts.first;
    this.lastName = parts.last;
  }
}

enum PersonType { student, employee }

class Student extends Person {
  String nickName;
  Student(
    String firstName,
    String lastName,
    this.nickName,
  ) : super(firstName, lastName);

  @override
  String toString() => "${getFullName()}, aka $nickName";
}

abstract class Person1 {
  String firstName;
  String lastName;
  Person1(this.firstName, this.lastName);
  String get fullName;
}

class Student1 extends Person1 {
  Student1(String firstName, String lastName) : super(firstName, lastName);
  //... other class members
  @override
  String get fullName => "$firstName $lastName";
}

class Student2 implements Person1 {
  late String nickName;
  @override
  late String firstName;

  @override
  late String lastName;

  @override
  // TODO: implement fullName
  String get fullName => throw UnimplementedError();
  
  @override
  String toString() => "$fullName, also known as $nickName";
}


class ProgrammingSkills {
  coding() {
    print("writing code...");
  }
}

class ManagementSkills {
  manage() {
    print("managing project...");
  }
}

// mixin sth, multiple inheritance
class SeniorDeveloper extends Person with ProgrammingSkills, ManagementSkills {
  SeniorDeveloper(String firstName, String lastName) :
  super(firstName, lastName);
}

class JuniorDeveloper extends Person with ProgrammingSkills {
  JuniorDeveloper(String firstName, String lastName) :
  super(firstName, lastName);
}


