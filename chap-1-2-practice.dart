void main() {
  // hello with for loop
  for (int i = 0; i < 5; i++) {
    print('hello ${i + 1}');
  }
  print('\n');
  
  // Variable declaration
  var inferredString = "Hello"; // Type inferred as String
  String explicitString = "World";
  print(inferredString.runtimeType);
  print(explicitString.runtimeType);
  print('\n');

  // Null Safety
  null_declare_and_calling();
  null_assert();

  // Lists
  operate_on_list();

  // Maps
  Map nameAgeMap = {"Vince":21};
  nameAgeMap["Alice"] = 23;
  print(nameAgeMap); // Prints 23

  // Strings
  practice_strings();

  // optional Optional positional parameter function
  print(sayHappyBirthday('Vince') + "\n");
 
  // lambda functions
  List lst = [1, 2, 3, 4];
  lst.forEach((number) => print('hello $number'));

}

sayHappyBirthday(String name, [int? age]) {
  return "$name is $age years old";

}

void practice_strings(){
  String singleQuoteString = 'Here is a single quote string';
  String doubleQuoteString = "Here is a double quote string";
  // multi-line string
  String multiLineString = '''Here is a multi-line single
  quote string''';
  // concatenate string
  String str1 = 'Here is a ';
  String str2 = str1 + 'concatenated string';
  print(str2); // Prints Here is a concatenated string
  // string interpolation
  String someString = "Happy string";
  print("The string is: $someString");
  // prints The string is: Happy string
  // No curly brackets were required
  print("The string length is: ${someString.length}");
  // prints The string length is: 16
  // Curly brackets were required
  print("The string length is: $someString.length");
  // prints The string length is: Happy string.length
  // Omitting the curly brackets meant only the variable was evaluated, not the method on the variable.
  print('\n');
}

void operate_on_list(){
  List dynamicList = [];
  print(dynamicList.length); // Prints 0
  dynamicList.add("Hello");
  print(dynamicList[0]);
  print(dynamicList.length); // Prints 1
  dynamicList.add(2);
  print(dynamicList);   
  List preFilledDynamicList = [1, 2, 3];
  print(preFilledDynamicList[0]); // Prints 1
  print(preFilledDynamicList.length); // Prints 3 
  print('\n');

  // Fixed size list
  List fixedList = List.filled(3, "World");
  //fixedList.add("Hello"); // Error
  fixedList[0] = "Hello";
  print(fixedList[0]); // Prints "Hello";
  print(fixedList[1]); // Prints "World";
  print('\n');
}

void null_declare_and_calling(){
  // using ? declaration
  int? newNumber; // newNumber type allows nullability
  print(newNumber); // Prints null
  newNumber = 42; // Update the value of newNumber
  print(newNumber); // Prints 42
  
  // using late variables
  late int newNumber1; // newNumber type allows nullability
  // Do some initialisation stuff
  newNumber1 = 42; // Update the value of newNumber
  print(newNumber1); // Prints 42
  
  // null aware method of calling nullable variables
  String? goalScorer;
  // Other code
  print(goalScorer?.length);
  print('\n');
}

void null_assert(){
  int? goalTime;
  String? goalScorer;
  bool goalScored = false;
  // Other code
  if (goalScored) {
    goalTime = 21;
    goalScorer = "Bobby";
  }
  // More code
  if (goalTime != null) {
   print(goalScorer!.length);
  }
  
}